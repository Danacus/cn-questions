# Layering and the Application Layer

## Layering (5 marks)

Identify which layer of the OSI network stack each of the following protocols belong in:

A. Real Time Protocol (RTP)

Either application layer or transport layer. This is debatable. Since it can be used by multiple different
applications it fits in the transport layer, but at the same time it also looks more like an application layer protocol.

B. Dynamic Host Configuration Protocol (DHCP)

Application layer

C. User Datagram Protocol (UDP)

Transport layer

D. Chord

The fuck is chord?

E. Ad-hoc On Demand Distance Vector Routing (AODV)

Network layer

## The Domain Name System (DNS) (8 marks)

This question concerns the Domain Name System:

A. Name the different types of elements that are present in the DNS system and define
their purpose.

- DNS records. These are entries in a table that can map human readable addresses on other addresses like IPv4 addresses.
- Name servers. A server that is responsible for a certain group of domains. For example the .xyz nameserver or a nameserver owned by a web hosting company.
  It receives queries from a local name server and responds to them.
- Top level domain (tld). The top level of a domain, like .xyz, .com or .be. Usually owned by a country or a company like DNS BE.
- Local name server. Handles DNS queries from a client like your phone. Usually built into a router/modem that connects a home to the internet, but can be any server in a network.

B. Define what is meant by the ‘iterative’ and ‘recursive’ phases of a DNS query.

Consider the following address: home.vanoverloop.xyz. My local DNS server will start with the top level: .xyz.
So it sends the query to a known IP address of the .xyz name server. The .xyz server will not recursively resolve
this address, it will only return the IP address of vanoverloop.xyz to the local name server. The local name server
will now iteratively continue by sending the query to the IP address of vanoverloop.xyz. That name server will
return the IP address of my home. Now the local name server can send the IP address to the to the client.

C. Explain why the iterative phase of DNS operation uses only UDP.

The local name server would have to make a lot of short lived connections to many different name servers.
Using TCP would have a lot of overhead from having to setup all these connections just to send a single query,
so UDP makes more sense.

D. Explain why the recursive phase of operation may use either UDP or TCP.

While in the recursive phase there is a persistent chain of connections from the client to the name
server that is iteratively resolving the address. So it is reasonable to use TCP. However,
since there isn't a lot of communication between the client and the local name server, UDP is
good enough.

E. Where is caching implemented

In the local name server. For example, a raspberry pi in the local network keeping a cache allowing 
clients within the network to quickly resolve DNS addresses. Or Cloudflare supposedly resolving
DNS queries faster than any other DNS server because it probably has a large and fast cache.

## Gnutella vragen

- Hoe word er gezocht?

In de eerste fase zoekt een nieuwe peer een aantal buren. Hij broadcast hiervoor een "ping" bericht.
Elke andere peer die graag buur wil worden stuurt een "pong" bericht terug.
Eens de peer deze buren heeft stuurt hij zijn queries naar deze buren. Die buren sturen de queries dan
verder naar hun buren. Zo wordt er gezocht naar een peer die het gevraagde bestand heeft.

- Wat zijn de verschillen voor de search in Gnutella 0.6 (tegenover 0.4)?

In Gnutella 0.6 worden ultra-peers gebruikt om discovery en queries te doen. Leaf nodes verbinden
dan met een ultra-peer die het zware werk in hun plaats doet.

- Wat is de impact van NAT op Gnutella?

NAT breekt het end-to-end model van computer netwerken. Hierdoor werken protocols zoals Gnutella niet meer aangezien
clients achter NAT geen inkomende connecties kunnen accepteren. Merk op dat wanneer ultra-peers gebruikt worden
dit eigenlijk geen probleem meer is. Ultra-peers staan niet achter NAT en clients achter NAT kunnen een verbinding
maken met een ultra-peer.

- Wat is de impact van een grotere TTL?

De broadcast in Gnutella 0.4 had een zeer grote overhead. Om files te vinden was een grote TTL value nodig,
maar dit zorgde voor zeer veel data die in het netwerk werd verspreid.

- Zou UDP een alternatief voor TCP kunnen zijn voor Gnutella? Verklaar.

Voor discovery en queries wel, maar niet voor de file transfer. Gnutella maakt gebruik van HTTP en heeft
de betrouwbaarheid van TCP nodig. Het is in principe wel mogelijk om een hoger laag protocol bovenop
UDP te bouwen, maar dit is niet zinvol.

- Welk van de twee versies is makkelijker om te monitoren? Verklaar je antwoord.

Gnutella 0.6 kan je makkelijker monitoren. Alles wat je moet doen is een zeer sterke ultra-peer maken
die veel connecties van clients accepteert. Zo kan je die clients monitoren, je weet namelijk exact welke bestanden
elke client probeert te downloaden.

## Bittorrent vragen (verzonnen)

- Hoe wordt er gezocht?

Torrents worden gezocht op gewone websites.

- Wat doen "trackers"?

De taak van trackers is het bijhouden van IP adressen van clients in de swarm van elke file.
Zo kunnen clients elkaar vinden.

- Hoe kan je clients monitoren?

Door in verschillende swarms te gaan kan je kijken welke IP adressen welke file downloaden.

## TOR

- Welke informatie heeft een entry routing node, een ending routing node en een intermediate routing node. 

De entry node heeft informatie over de client die een connectie wil maken. Het weet niet met welke server de client wil verbinden.
De intermediate node weet enkel dat er een bericht gaat van de entry node naar de ending node. Het weet niets over de client en de server.
De ending node weet naar met welke server er verbinding gemaakt wordt, maar weet niets over de client. Indien er geen encryptie van de applicatielaag
is door bijvoorbeeld HTTPS, kan de ending node ook de originele data van het bericht lezen.

- Leg de werking van TOR relay uit en waarom is het nodig?

Een Tor cell wordt versleuteld met 3 sleutels. Elke sleutel voegt een laag van encryptie toe. Elke relay kan precies 1 laag van de cell ontcijferen
en geeft het resultaat telkens door aan de volgende relay. Dit is noodzakelijk om ervoor te zorgen dat geen enkele relay node weet welke client
met welke server verbinding maakt. De entry en intermediate nodes weten niets over de server aangezien dit nog versleuteld is met de sleutel van de ending node.
De ending node weet echter niets meer over de client aangezien de andere nodes deze informatie niet hebben doorgegeven. Het is dan ook belangrijk dat elke
relay node onafhankelijk is. Indien 3 nodes overgenomen worden dan is het netwerk niet meer helemaal prive.

- Hoeveel intermediate nodes zijn er minstens nodig om veilig te zijn?

Minstens 1. In principe heb je geen intermediate node nodig, maar het is een te groot risico als er maar 2 nodes overgenomen moeten worden door een entiteit 
met slechte bedoelingen om de privacy te doorbreken.

## DHCP

- Leg werking uit (in diagram of tekstvorm). 

1. Een client broadcast een DHCPDISCOVER bericht over het netwerk.
2. Elke DHCP server in het netwerk kan een IP adres voorstellen met een DHCPOFFER bericht.
3. De client stuurt een DHCPREQUEST bericht om het adres te accepteren en de server antwoord met DHCPACK.
4. De client gaat nog even na of het IP adres wel degelijk vrij is.
5. De lease kan verlengd worden met een DHCPREQUEST.
6. Met DHCPRELEASE kan het IP adres terug vrijgegeven worden.

- Zou dit ook kunnen werken op TCP?

Nee, DHCP maakt gebruik van broadcast en wordt gebruikt op het moment dat de client nog geen IP adres heeft.
TCP kan dus nog niet gebruikt worden, want hiervoor heb je een IP adres in het netwerk nodig.

## HTTP

Wat zijn de voordelen van HTTP 1.1 over 1.0 en bij welk voorbeeld zijn deze voordelen duidelijk zichtbaar? 

- Persistant connections: bij HTTP 1.1 kan je een connectie open houden zodat je meer bestanden kan downloaden in dezelfde connectie.
Dit is vooral handig bij een website met veel afbeeldingen.
- Chunked data transfer: bij HTTP 1.1 kan je een bestand in stukken downloaden. Het is dan gemakkelijker om een download te pauzeren en later te hervatten.

## Extra vragen

- Hoe zou je DNS testen met Telnet? Verklaar. Bijvraag: Hoe zou je DNS dan wel kunnen testen?

Je kan DNS niet testen met telnet want telnet gebruikt TCP en DNS gebruikt UDP. Om DNS te testen moet je UDP segments sturen naar een DNS server, je kan dit gemakkelijk doen met een tool zoals `dig`.

# Transport Layer

## RTP

- The RTP header is shown below:

![](transport-000.png)

Briefly explain the purpose of each of the 10 header fields shown in the diagram above.

Boring question... see slides

- Vergelijk de sequencing van RTP en TCP. Waarom is er een verschil? 

RTP houdt geen rekening met verloren segmenten. Er zijn geen hertransmissies van vorige segmenten en elk segment
wordt zo snel mogelijk verstuurd. TCP zal verloren segmenten opnieuw versturen.

- Is RTP meer geschikt voor het downloaden van een video file dan TCP? Waarom? 

Nee, dat is een zeer slecht idee. Wanneer je een video download dan maakt het niet echt uit of de jitter hoog is of
het iets langer duurt. Het is belangrijker dat er geen fouten in de video zijn.

- Welk probleem kom je tegen bij een netwerk met high-volume RTP traffic, dan je niet hebt bij TCP?

Als er congestion is zullen er mogelijks veel segmenten verloren gaan bij RTP. RTP gebruikt UDP en er is
dus geen throttling en er worden geen segmenten opnieuw verstuurd. Dit zal een grote invloed hebben op
de kwaliteit van de video. Wanneer TCP gebruikt wordt doet dit probleem zich niet voor omdat TCP automatisch
zal vertragen bij congestion en verloren segmenten opnieuw zal versturen.

## Sliding Window Protocols

A. What is a Negative ACKnowledgement (NACK) and when would it be used?

A negative acknowledgement is used in sliding window protocols. In can be used when the checksum does not match and
the segment is corrupted. This is mainly relevant for protocols like stop-and-wait and not for TCP, because
TCP uses an acknowledgement number and this number can be used to tell the sender that you did not receive a certain segment.

B. Describe the constraints on window size that arise from sequence number range.

The window size can never be larger than the largest sequence number. If this were the case, a sequence number of for example 3 might
refer to the third segment in the window or to a segment near the end of the window. This could cause problems with ordering and/or acknowledgement.

C. Under what circumstances will a stop-and-wait protocol perform the worst?

When the latency is very high. Suppose you would send a segment to the other side of the world. You would have to immediately wait for
the acknowledgement to return. So you would be waiting twice the latency after each sent segment. This is not a big problem when you are
for example using a bus which generally has very low latency.

## Silly Window Syndrome

Define the ‘silly window syndrome’. What technique(s) did we study to avoid this problem?

In case the receiver reads from its buffer one byte at a time it could send an acknowledgement with a window size of 1 after each
read byte. That would mean the sender would have to send a segment with more than 100 bytes just to send a single byte. This is silly.
Instead we can use Clarke's algorithm to only send the new window size after the window size is greater than the MTU or the window buffer is half empty.

## TCP

- Gegeven de TCP header: verklaar de 15 velden.

Saaie vraag...

- Vergelijk TCP flow control met go-back-N. Geef verschillen en gelijkenissen.

Het zijn beide sliding window protocols.
TCP is gebaseerd op go-back-N. Er is een window size en een acknowledgement number wordt gebruikt
om eventueel om hertransmissies te vragen van de zender. In TCP kan de window size wel dynamisch
veranderen. De ontvanger laat weten hoeveel plaats er is in de buffer. Dit is standaard niet
het geval bij eenvoudige go-back-N.

## Tinygram syndrome

Wat is het 'Tinygram syndrome' en welke oplossingen hebben we hiervoor gezien?

Als er bij de zender data byte per byte in de buffer komt dan kan deze data byte per byte verstuurd worden.
Dit is nogal inefficient aangezien elk TCP segment al meer dan 100 bytes is. We kunnen dit oplossen met
Nagle's algorithm door zolang er nog een segment zonder ack verstuurd is de data te bufferen tot we een volledig
segment hebben. Als we wel al een ack hebben gekregen van het vorige segment dan kunnen we beter alle data
die we nog hebben versturen. We weten immers niet of er nog meer data gaat komen of niet.

## TCP sockets

Beschrijf de werking van TCP sockets in Python.

Deze interface komt vrij goed overeen met de onderliggende POSIX C interface.

- socket: maakt een nieuwe TCP connectie
- connect: maakt verbinding met een server die aan het listenen is.
- recv: leest een vaste hoeveelheid data van de socket
- bind: bind de socket aan een poort
- listen: luister op een poort voor inkomende connecties

## Retransmission

Leg uit waarom het goed is dat deze in de Transport Layer gebeuren. Geef ook voordelen van retransmission in de Data Link Layer.

Enkel retransmission in de datalinklaag volstaat niet aangezien packets ook in de netwerklaag verloren kunnen gaan door een congested router bijvoorbeeld.
Daarom is het goed dat dit ook in de transport layer aanwezig is. Toch is het handig om dit ook in de datalinklaag te hebben.
Een draadloos medium zoals IEEE 802.11 is bijvoorbeeld niet altijd even betrouwbaar. In sommige gevallen is het in de datalinklaag ook al onmiddelijk
duidelijk dat er bijvoorbeeld een collision is. Het is dus handig om dat al een hertransmissie te doen in plaats van te wachten tot de timer in
de transportlaag afloopt. Ook voor UDP biedt dit voordelen aangezien UDP geen hertransmissies doet en het niet goed zou zijn als het medium
invloed heeft op de betrouwbaarheid van UDP. Als er geen hertransmissies in de datalinklaag zouden zijn dan zou live video over IEEE 802.11
er mooi uitzien :)

# Network Layer

## Ad-hoc On Demand Distance Vector Routing (AODV)

When would you choose to apply the AODV routing protocol instead of Open Shortest Path First (OSPF)? Justify your answer.

The main advantage of AODV is that it's computationally cheaper to add or remove devices from the network. It's also better suited for
relatively large and volatile networks, because it computes a path on demand, whenever a datagram is sent. OSPF is better for
a fixed set of routers that don't change very ofter, because it uses link state routing to find the sink tree, which is more computationally
expensive. It's also harder to deal with failing nodes, while AODV will automatically notify neighbouring nodes when a node fails.

## The Count to Infinity Problem

A. What is the ‘count to infinity’ problem?

When using distance vector routing like the Bellman Ford algorithm, good news will travel quickly. New paths using a new router
will be found quickly. However, bad news travels very slowly. Consider the following set of routers, the number represents the distance
to router A.

A   B   C   D   E
0   1   2   3   4

Now router A will fail, causing other routers to update their distances.

x   3   2   3   4

B thinks it can still reach A, because C was able to reach A. However, what B doesn't realise is that C was able to reach A through B.
This continues:

    3   4   3   4
    5   4   5   4
    5   6   5   6
    
The distances are all counting to infinity and this is a big problem.

B. What techniques does AODV use to address this problem?

Whenever a node fails all of its neighbours will notify their neighbours which will notify their neighbours until every node has heard the bad news.
Every node will drop all paths that use the node that failed and might have to compute some new paths whenever the next datagram is to be sent.

## IP Addresses

A. What type of IP address is this: 172.16.0.1 ?

This is an IPv4 address.

B. What is the broadcast address of subnet 172.29.206.0 (mask 255.255.254.0)?

The broadcast address is 127.29.207.255 (The last bit of 206 is 0, but we want to make it 1, so we add 1. The last byte should be all ones, so 255)

C. What is the broadcast address of subnet 172.23.193.64 (mask 255.255.255.192)?

```
Mask:
192 = 1 * 128 + 1 * 64      = 0b11000000

Original:
64  = 0 * 128 + 1 * 64      = 0b01000000

Broadcast:
0xFF - 0b10000000 = 255 - 128 = 127
                            = 0b01111111 = ((!mask) | original)
```

The broadcast address is equal to 172.23.193.127

D. What is the first valid host of subnetwork 172.22.7.16/28?

The first host is 172.22.7.16

E. What is the last valid host of subnetwork 172.22.7.16/28?

```
16 = 0 * 128 + 0 * 64 + 0 * 32 + 0 * 16 = 0b00010000
                                            ^^^^
                                            Net Mask
```

So the last byte of the broadcast address would be 0b00011111, which makes 0b00011110 the last byte of the last valid host.

```
0b00011110 = 16 + 8 + 4 + 2 = 30
```

So the last valid host is 192.22.7.30

## Subnets invullen

![](https://wiki.wina.be/images/examens/SchemaExamen14Juni2021.png)

Ik weet eigenlijk niet wat de vraag is :)

## Overige vragen

- MTU en ipv4 fragmentatie zijn incompatible. Waarom?

Bij MTU probeer je te achterhalen wat de grootste mogelijke lengte van een pakket is. Dit gebeurt door incrementeel grotere 
pakketten te sturen tot het fout loopt. Maar als er fragmentatie gebruikt wordt, dan zal het niet fout lopen wanneer het pakket
te groot is, het pakket wordt dan transparant gefragmenteerd en de client die de MTU wil bepalen merkt hier niets van.

- Wat is RED? En hoe werkt het?

Random Early Detection is een manier om om te gaan met congestion. Het werkt als volgt: Wanneer er bijna congestion is zal de router
willekeurig pakketten droppen. Deze gedropte pakkets worden gebruikt als metriek in de transportlaag om te vertragen.
Het handige is dat de kans het grootste is dat een pakket wordt gedropt van het device dat de meeste pakketten stuurt. Zo zorg je
voor een soort van balans met deze strategie.

- Definieer fragmentation (network layer) en segmentation (transport layer). Waarom is er een verschil? 

Fragmentation is het splitsen van pakketten in kleinere fragmenten. Segmentation is het splitsen van segmenten in kleinere segmenten.
Fragmentation gebeurt binnen het netwerk door routers terwijl segmentation gebeurt door de eindpunten in de transportlaag.

- Gaan packets door de netwerk layer allemaal op dezelfde manier? 

Nee, ze kunnen een ander pad volgen of door een ander medium gaan. Bij een connection-oriented aanpak zouden alle pakketten van
dezelfde connectie hetzelfde circuit volgen, maar in IP worden er geen connecties gemaakt in de netwerklaag en wordt elk pakket
apart verstuurd door het netwerk.

- Wat is de rol van ICMP bij MTU?

ICMP pakketten kunnen gebruikt worden om de MTU te vinden. Routers sturen dan een foutmeldingspakket terug wanneer de pakketgrootte
groter is dan de MTU.

- Leg Round Robin fair queuing uit. Hoe kan een host hiervan misbruik maken? 

Elk device verbonden met een router krijgt een eigen queue in die router. De router neemt dan afwisselend een pakket van elke queue.
Een host kan dit misbruiken door zeer grote pakketten te gebruiken, want dan krijgt die host meer throughput dan de anders hosts die kleinere pakketten gebruiken.

- Geef 2 van de 3 mogelijkheden voor congestion notification en vergelijk de voordelen en nadelen van beide. (10p)

Je kan choke packets sturen naar de zender om aan de transportlaag te melden dat het trager moet gaan.
Het voordeel is dat dit snel gaat, maar het nadeel is dat er extra bandbreedte wordt verspild.
Een andere optie is Explicit Congestion Notification waarbij bestaande pakketten geflagd worden indien
er congestion is. Dit versplilt minder, maar is trager. Nog een andere optie is Random Early Detection
waarbij willekeurige pakketten gedropt worden. Het voordeel hiervan is dat het heel simpel is,
hosts gebruiken packet loss als metriek om te throttlen. Een ander voordeel is dat het statistisch waarschijnlijker
is om een host te straffen die het meeste pakketten stuurde. Het nadeel is dat dit random is en je ook toevallig
een host kan straffen die amper pakketten aan het sturen was.

- Adressering: Vergelijk adressering in Network Layer met die in Data Link en Application Layer. Geef aan waarom dit nodig is. Bespreek uitvoerig. 

In de netwerklaag worden unieke adressen gebruikt, in tegenstelling tot TSAPs in de transportlaag. Dit is nodig omdat deze adressen gebruikt worden
om een andere host te vinden in het netwerk. We kunnen ook niet zomaar MAC adressen gebruiken aangezien MAC adressen niet noodzakelijk uniek zijn 
en geen hierarchie hebben. IPv4 adressen zijn hierarchisch en dit is belangrijk om efficient een andere host te kunnen vinden op de hele wereld.
Er is nood aan een zekere structuur in tegenstelling tot in de datalinklaag.

# Data Link Layer

## Security and Privacy

What are the security and privacy implications of using each of the following networks:

A. Classic Ethernet

There is no security or privacy, every host can see messages from every other host.

B. Switched Ethernet

No security or privacy problems because the switch will only send frames to the destination. Other hosts will not receive these frames,
because every host is connected to a router or switch using its own cable. Of course, someone could plug your cable into a malicious router,
but that's unlikely.

C. 802.11 (WiFi)

No real security here either, because every host yells their data through the room. A malicious node can easily listen to the data.
In order to make this more security, encryption like WPA2 can be used.

Your answer should assume that no higher layer security schemes are applied.

## Ethernet Cable Length Limits

Is switched Ethernet subject to the same cable length limitations as classical Ethernet?
Justify your answer.

There should not be any limitations because there are no collisions when using duplex twisted pair wires. However, in order
to maintain backwards compatibility with the original protocol, which includes collision detection, a compromise between speed
and cable length had to be made. When we increase the rate at which we send data, the maximum length is reduced, because otherwise
we wouldn't be able to detect collisions correctly.

## Frame Loss on Wireless Networks

A. Explain the key sources of frame loss at the data link layer.

Interference from other devices is a big cause of frame loss. The path can also be blocked by walls or large volumes of water.

B. Will frame loss on an 802.11 (WiFi) network interact with the congestion control mechanism of TCP? Justify your answer.

It will indirectly through the network layer. When there is a lot of frame loss in the data link layer, many packets may add up in the network layer
waiting to be transmitted in frames. This causes the network layer to notify the transport layer about congestion. TCP will throttle when
it receives such signal. But note that there is no direct interaction between the data link layer and the transport layer.
The transport layer won't know the difference between congestion in a router due to a lot of different traffic or due to frame loss in the data link layer.
It doesn't even know that frame loss is a thing, it is shielded from this.

## Leg de twee manieren van power saving in 802.11 uit en geef een voorbeeld van een toepassing die goed past bij elke manier.

1. Door beacons te gebruiken. Een Access Point kan op een vast tijdsinterval beacons sturen naar de 802.11 transceivers.
   Wanneer een transceiver zo een beacon ontvangt zal hij voor een korte periode luisteren.
2. Door de clients zelf af en toe een bericht te laten sturen. De AP kan data voor de host bufferen tot de client zelf een bericht stuurt.
   Na het sturen van een bericht luistert de client nog even voor nieuwe berichten. Dit werkt goed in sensornetwerken waarbij er data van sensoren
   wordt verzameld. De sensoren sturen dan data wanneer ze nieuwe data hebben.
    
## Hoe komt het dat een switch efficienter is dan een hub (voor Ethernet)

In een hub worden alle ethernet kabels met elkaar verbonden alsof het 1 kabel is. Elke host ontvangt dan alle data van alle andere hosts.
Dit wil zeggen dat als host A iets stuurt naar host B, host C dit ook zal ontvangen. Als host C dan iets naar host D wil sturen moet hij 
wachten tot host A klaar is met data naar host B te sturen. Bij een switch wordt de data van host A in dit voorbeeld enkel naar host B
gestuurd en kan host C tegelijkertijd met host D communiceren. Dit is efficiënter.

## Berkeley MAC vragen:

- Op welke manier beperkt de lengte van de preamble het polling interval?

De lengte van de preamble moet minstens even lang zijn als het polling interval. Stel dat dit korter zou zijn en net nadat host A gepollt heeft
wordt er een nieuw bericht gestuurd door host B. Wanneer host A voor de volgende keer pollt na het interval, dan is de preamble al voorbij
en heeft host A een deel van de data gemist. Dit is niet goed en dit kan niet gebeuren als de preamble lang genoeg is. De preamble bevat immers
geen nuttige data, het bestaat enkel om andere nodes te laten weten dat er een nieuwe transmissie is.

- Hoe zou je BMAC configureren voor een netwerk met lage data rates?

Ik zou een lang polling interval gebruiken zodat het weinig energie kost om te luisteren. Het nadeel is dat het dan meer energie kost om
data te verzenden aangezien er een lange preamble moet zijn, maar dit is geen groot probleem als de data rates laag zijn.

- Hoe zou je BMAC configureren voor een netwerk met hoge data rates?

Bij hoge data rates zou ik het omgekeerde doen. Een lage polling interval zorgt dat de preamble korter is en de goodput dus hoger is.
Het is niet zo een groot probleem dat er veel energie wordt verspild aan het luisteren aangezien er sowieso toch veel geluisterd moet
worden want er wordt veel data verstuurd.

## Hidden terminal problem

- Wat is het hidden terminal problem? 

Beschouw de volgende figuur:

A <----------> B <---------------> C
             <------------------------>
                    radio bereik

De radio van C bereikt A niet, waardoor A niet merkt dat B bezig is met een transmissie van C te ontvangen. 

- Hoe wordt dat opgelost met de MAC protocols (geef 2 voorbeelden)? 
    - p-persistant CSMA: Wanneer B bezig is dan zal A het bericht herverzenden met kans p.
    - exponential back-off: Wanneer er een collision is dan wachten beide nodes een willekeurige hoeveelheid
      tijd voordat ze hun frame herverzenden. Deze tijd stijgt exponentieel. Dit wordt gebruikt in IEEE 802.3 (Ethernet) en IEEE 802.11 (Wi-Fi).
 
- Wat is het exposed terminal problem? 

Het kan ook dat A een bericht aan het versturen is, waardoor B denkt dat hij geen bericht kan sturen naar C, want B denkt dat er al een andere node aan het sturen is die dit zou verstoren.

- Kan CSMA hierbij helpen? 

Nee, bij CSMA is er pas hertransmissie wanneer het channel idle is, maar dit is in deze situatie niet het geval.

- Wat zijn de voordelen van channel hopping?

Channel hopping zou het exposed terminal probleem oplossen. Wanneer A op het ene channel aan het transmitten is kan B een ander channel gebruiken.
Dit verlaagt ook de kans op interference in multi-hop networks. Een voorbeeld hiervan is Time Slotted Channel Hopping (TSCH) (niet in cursus maar leuk weetje :) ).

## ALOHA

- Beschrijf pure ALOHA in zo veel mogelijk detail. 

Bij pure ALOHA kan elke node beginnen met versturen op elk moment. Wanneer er een collision is, dan is de frame corrupt. Dit wordt gedetecteerd en dan zullen de nodes
na een willekeurige hoeveelheid tijd hun frame opnieuw versturen.

- Vergelijk de performantie met slotted ALOHA.

Het probleem met pure ALOHA is dat er vaak collisions zijn, want een frame kan op elk moment verstuurd worden. 
Wanneer je transmissies start in vaste time slots is de kans op een collision kleiner. Dus ondanks er langer
gewacht moet worden tot de start van een time slot, is de performantie beter aangezien er minder collisions zijn.

## Leg TSMP uit.

Time Synchronised Mesh Protocol. Om energie te besparen is het interessant om enkel op vaste tijdstippen frames te versturen, 
want dan moeten clients enkel op die momenten hun radio aanzetten om te luisteren. TSMP doet dit door bij elke nieuwe
node in het netwerk eerst de klok te synchroniseren. Deze klok wordt bepaald door een controller. De sensor luistert hiervoor even naar de klok
en stelt dan een precieze hardware klok in. Deze fase kost het meeste energie, maar daarna is de kost veel lager.
Wanneer er een transmissie gestart wordt op zo een tijdstip, dan kan een node ervoor kiezen om te blijven luisteren
tot het einde van die transmissie. TSMP ondersteunt ook frequency hopping om verder collisions te vermijden.

## Low Power Listening
 
2 situaties van low power listening: 

1. een systeem waarbij sensoren vrije plaatsen in een parking meten
2. sensoren aan vaten met chemische stoffen, die niet bij elkaar in de buurt mogen komen, anders starten ze een alarm. 

Geef voor beide een MAC protocol dat er het best bij past en leg uit.

1. Er is in deze situatie sowieso een centrale controller die data verzamelt. Deze controller kan een klok bepalen zodat de sensoren enkel
   hun radio moeten aanzetten op vaste tijdstippen. Een protocol zoals TSMP lijkt mij geschikt voor deze situatie aangezien er toch al een 
   centrale controller is en er niet snel nodes worden toegevoegd of verwijderd van het netwerk. De meest kostelijke stap, het toevoegen
   van een nieuwe node aan het netwerk gebeurt dus niet vaak.

2. Dit is een eerder gedecentraliseerde situatie, dus lijkt TSMP mij minder geschikt. Berkeley MAC zou hier wel goed kunnen werken.
   Een relatief lang interval kan gebruikt worden om energie besparen. Het is niet erg dat de preamble dan relatief lang is
   aangezien er toch niet zo veel traffic is. Zolang het maar kort genoeg is om op tijd alarm te starten. Als optimalisatie
   zouden we een bewegingssensor kunnen toevoegen aan elk vat. Enkel wanneer er beweging is moet de radio data versturen.
   Zo wordt er energie bespaard wanneer de vaten niet bewegen, aangezien er dan niets verstuurd wordt en er maar weinig geluisterd moet worden.

## Packet loss in 802.11

Zou packet loss goed zijn om congestion aan te duiden in een 802.11 netwerk? Leg gedetailleerd uit. (7p)

Ja, dit lijkt mij een goed signaal. Wanneer er problemen zijn met het versturen van frames door bijvoorbeeld interferentie,
dan zullen pakketten zich opstapelen in de netwerklaag. Ofwel zal kan de router dan zelf pakketten droppen, ofwel
kan het pakket niet verstuurd worden omdat er te veel storing is en dan moet het pakket ook gedropt worden.
Packet loss is dus een goed signaal voor de transportlaag om aan te geven dat er ergens congestion is, mogelijks in de 802.11 transmissie.

## Byte stuffing

Leg uit waarom byte stuffing nodig is. Geef een numeriek voorbeeld waar bit stuffing efficiënter is dan byte stuffing. (7p)

Wanneer je de lengte van een frame wil aangeven is het een slecht idee om een veld in de header te gebruiken. Het is mogelijk dat
dit veld gemist wordt en dat andere bit geïnterpreteerd worden als lengte. Op dat punt is de ontvanger helemaal kwijt
waar een frame begint en waar een frame eindigt. Het is dus beter om een bepaalde flag byte of sequentie bits te gebruiken om
een start of stop aan te duiden. Stel dat de volgende byte de flag is: 01111110. Indien we deze byte als data willen versturen
en niet om start of stop aan te duiden, dan moeten we die byte escapen. Daarvoor kunnen we een escape sequence gebruiken, noem deze byte ESC.
Dan kunnen we ESC voor 01111110 zetten om die byte als data te beschouwen. Dit is echter vrij inefficient, daarom kunnen we bit stuffing gebruiken.
Telkens wanneer we 5 opeenvolgende enen zien kunnen we een 0 toevoegen. Om 01111110 te versturen als data maken we er 011111010 van. Dit
zijn veel minder bits dan ESC + 01111110.

## Switch vs Hub

Stel dat hubs en switches evenveel kosten. Geef een situatie waarbij een hub beter zou zijn dan een switch bij het toevoegen van een nieuwe host op het netwerk. (4p)

Wanneer je baas bent op een kantoor en je werkende slaafjes wil controleren door hun netwerkverkeer af te luisteren.

## Overige vragen

- Ethernet: Leg uit waarom er een beperking is bij klassieke Ethernet in kabellengte. Is deze hetzelfde voor a) Fast Ethernet met hub, b) Fast Ethernet met switch. Leg uit.

Ethernet heeft een maximale rate waarop data verstuurd kan worden. Wanneer er een collision is moet die collision gedetecteerd worden voordat er nieuwe
data verstuurd wordt. Hoe langer de kabel, hoe langer het duurt voor het signaal om door de kabel te gaan en hoe langer het kan duren om een collision
te detecteren. Voor een zekere snelheid is er dus steeds een maximale lengte van de kabel.

Bij Fast Ethernet met hub doet hetzelfde probleem zich voor, maar de kabellengte is nu nog beperkter aangezien de snelheid hoger is.
We moeten dus kunnen garanderen dat collisions sneller gedetecteerd kunnen worden door de kabellengte te beperken.

Bij Switched Ethernet is dit geen probleem meer aangezien er geen collisions meer kunnen voorkomen. We veronderstellen hier wel
dat de kabels tussen hosts en switches full duplex zijn.

- Kan het Pure ALOHA algoritme ook werken op 802.11? Verklaar.

Misschien, het is in ieder geval een slecht idee.
